## Local CricBuzzinga
---------------------

Data Modeling for Cricket Game.
(Check the image `cricket_models.png`)
Table definations in cricbuzz/dashboard/models.py

![cricket_modeling](https://bytebucket.org/sagarrakshe/cricbuzzinga/raw/526c7420e61308be7bd975f3b773b47db67310eb/cricket_models.png)
Format: ![Alt Text](url)



## FormatPlayer
The is to map the captain (or vice-captain) for various formats.


## Player
`role` of player can be batsmen, bowler, all-rounder.


## Matches
Matches is also used innings of test, hence linked to self. In other format,
parent_match will have -1 value in case of Test, the innings will link to the 
parent record. Each inning will behave same as match, it will have result.

NOTE: Now when we derive stats from Match table we need to keep in mind the self-link.
Total tests played by a team will not involve innings, just need to consider the
parent_matches.


##  BallCount
Each ball bowled is recorded here. Bowled by and bowled to, and the runs scored.
ball_count fields is incremented for every ball, valid_ball_count is only 
incremented if the ball is valid (no extra). Wicket will be incremented accordingly.
We can derive total sixes and fours hit by a player from these tables.


## Todo
---------

* Capturing wickets in detail, type of wicket - bold, run-out, caught etc.
* Player can be in other team based on format (eg. IPL)

