# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-29 14:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0011_auto_20171129_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='captain',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='captain', to='dashboard.FormatPlayer'),
        ),
        migrations.AlterField(
            model_name='team',
            name='vice_captain',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vice_captain', to='dashboard.FormatPlayer'),
        ),
    ]
