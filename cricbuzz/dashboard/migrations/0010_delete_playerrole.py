# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-29 14:08
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0009_auto_20171129_1407'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PlayerRole',
        ),
    ]
