# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible  # only if you need to support Python 2
class MatchFormat(models.Model):
    format_type = models.CharField(max_length=30)

    def __str__(self):
        return self.format_type


@python_2_unicode_compatible  # only if you need to support Python 2
class Series(models.Model):
    name = models.CharField(max_length=30)
    startdate = models.CharField(max_length=30)
    enddate = models.CharField(max_length=30)
    matches_count = models.IntegerField()
    sponsor = models.CharField(max_length=30)
    man_of_series = models.ForeignKey('Player', null=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class FormatPlayer(models.Model):
    format_id = models.ForeignKey(MatchFormat)
    player_id = models.ForeignKey('Player')

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class Team(models.Model):
    team_name = models.CharField(max_length=30, null=True)
    coach_name = models.CharField(max_length=30)
    captain = models.ForeignKey(FormatPlayer, related_name='captain')
    vice_captain = models.ForeignKey(FormatPlayer, related_name='vice_captain')

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class Player(models.Model):
    name = models.CharField(max_length=30)
    role = models.CharField(max_length=30)
    team_id = models.ForeignKey(Team)
    total_wickets = models.IntegerField(default=0)
    total_runs = models.IntegerField(default=0)
    total_catches = models.IntegerField(default=0)
    total_matches_played = models.IntegerField(default=0)

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class Matches(models.Model):
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=30)
    match_result = models.CharField(max_length=30)
    team1 = models.ForeignKey(Team, related_name='team1')
    team2 = models.ForeignKey(Team, related_name='team2')
    match_format = models.ForeignKey(MatchFormat)
    man_of_match = models.ForeignKey(Player)

    # Store string for sake of simplicity
    toss_won = models.CharField(max_length=30)
    toss_decision = models.CharField(max_length=30)

    # Store names for sake of simplicity
    umpire = models.CharField(max_length=30)
    third_umpire = models.CharField(max_length=30)

    first_to_bat = models.ForeignKey(Team, related_name='first_bat')
    series_id = models.ForeignKey(Series)

    # Self-Link
    parent_match = models.ForeignKey('self', null=True, default=-1)
    inning_count = models.IntegerField()

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class Scoreboard(models.Model):
    match_id = models.ForeignKey(Matches)
    player_id = models.ForeignKey(Player)
    score = models.IntegerField()
    wickets = models.IntegerField()

    # status wether is not-out, out, retired-hurt
    status = models.CharField(max_length=30)

    def __str__(self):
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class BallCount(models.Model):
    match_id = models.ForeignKey(Matches)
    team_id = models.ForeignKey(Team)
    run = models.IntegerField()

    # Incrementing value starting from 1
    wicket = models.IntegerField()

    # Incrementing value for every ball starting with 1
    ball_count = models.IntegerField()

    # Incrementing value only if the ball is valid, no extras
    valid_ball_count = models.IntegerField()

    # Extra balls will increment accordingly
    no_ball = models.IntegerField()
    wide_ball = models.IntegerField()
    free_hit = models.IntegerField()

    batsmen = models.ForeignKey(Player, related_name='batsmen')
    bowler = models.ForeignKey(Player, related_name='bowler')

    def __str__(self):
        return self.format_type


@python_2_unicode_compatible  # only if you need to support Python 2
class TeamRanking(models.Model):
    team_id = models.ForeignKey(Team)
    format_id = models.ForeignKey(MatchFormat)
    rank = models.IntegerField()

    def __str__(self):
        return self.rank


class PlayerRanking(models.Model):
    player_id = models.ForeignKey(Player)
    format_id = models.ForeignKey(MatchFormat)
    batting_rank = models.IntegerField()
    bowling_rank = models.IntegerField()

    def __str__(self):
        return self.batting_rank
